<?php
  require_once "session.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#000000" />
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">






      	<?php

        require_once "support.php";

        echo renderMenuBar();

        echo "<div class=\"container\">";
        echo "<div class=\"row\">";

			require_once 'SQLFunctions.php';
			$db = new SQLFunctions("localhost", "id2131024_admin", "sprcoredump", "id2131024_pixel");
			#$categoryID = 1;

			if(isset($_GET['id'])){
				$userID = $_GET['id'];
				$images = $db->getImagesByUser($userID);
			}else{
				$userID = 1;
				$images = $db->getImagesByUser($userID);
			}
			if(isset($_GET['pageNumber'])){
				$pageNumber = $_GET['pageNumber'];
			}else{
				$pageNumber = 1;
			}
			$user = $db->getUserInfo($userID);
			$profilePic = $user['imagePath'];
			$userName = $user['username'];
			$nextPage = $pageNumber + 1;
			$prevPage = $pageNumber - 1;
			$endIndex = $pageNumber *9;
			$count =mysqli_num_rows($images);
			$html = "<div class='new-image-divider'><br/><p><strong>$userName's Profile</strong></p></div><br/>";
			$html = $html."<div class='col-*-*'><center><img class='profile-image' src='$profilePic'></center></div><br/>";
			$html = $html."<div class='profile-section-header'><p>$count Uploads</p></div><div class='col-*-*'>";
			if ( $count > 0) {
				$iter = 0;
				while($row = mysqli_fetch_assoc($images)) {
					if($iter < $endIndex){
						if($iter >= ($endIndex - 9) ){
							$path = $row["path"];
							$id = $row["pictureID"];

							if($iter%3 == 0 && $iter !=0){
								$html = $html . "</div><div class='col-*-*'>";
							}
							$html = $html . "<a href='/viewpost.php?imageID=$id'><img class='grid-image' height='100' src='$path'></a>";

						}
					}else{
						break;
					}
					$iter++;
				}
			}else{

			}
			echo $html . "</div><div style=\"float: right; color: white;\"><a href='profileview.php?id=$userID&pageNumber=$prevPage'>Previous Page</a> <strong style='color:white;'> | Page $pageNumber  | </strong>
			<a href='profileview.php?id=$userID&pageNumber=$nextPage'> Next Page&nbsp</a></div>";
      $db->closeConnection();
    ?>
        <br/>
		</div>
    </div>
    <script src="index.js"></script>
    </html>

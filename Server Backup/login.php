
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#000000" />
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">

<nav class="navbar navbar-inverse navbar-fixed-top">

  <ul class="nav navbar-nav">
    <li><a href=""><img src="dennis.jpg" width="57" height "57"></a></li>
    <li><a href=""><strong>
      <logo><span style="color:white">Pixel</span></a></strong></logo></li>
      <li class="camera-icon">
          <div class="image-upload">
            <label for="file-input">
              <img src="camera.png" height="30" width="30"/>
            </label>

            <input id="file-input" type="file" accept="image/*" capture="camera">
          </div></li>
        <li class="menu-icon"><a href="index.html"><img width="30" height="30" src="browse.png"></a></li>
        <!--<li class="menu-icon"><a href=""><img width="30" height="30" src="search.png"></a></li>-->

      </ul>
    </nav>


    <div class="container">
      <div class="row">
        <div class="new-image-header">
          <p><strong>Login</strong></p>
        </div>

        <!--<div class="col-*-*"><img class="new-image-preview" src="<?php echo $_FILES["fileToUpload"]["name"]; ?>"></div>-->

        <div class="login">
            <!--<formtitle></formtitle><br/>-->
            <br/><formtitle>Username</formtitle><br/>
            <input type="text" class="form-text" name="username" id="username" style="width:100%" required><br/>
            <br/>
            <formtitle>Password</formtitle><br/>
            <input type="password" class="form-text" name="password" id="password" style="width:100%" required><br/>
            <br/>
           <input type="button" class="form-button" value="Login" onclick="validateData()" name="loginButton" id="loginButton" style="width:100%"><br/>
          <br/>
        </div>
      </div>
    </div>
	<script>

		function main() {
		    document.getElementById("loginButton").onclick = validateData; // DO NOT PUT ()
		}
		function validateData() {
			var alertMessage = "";
			var username = document.getElementById("username").value;
			var password = document.getElementById("password").value;
			if (username == null || password == null) {
				alert("Username and password fields cannot be empty.");
				return false;
			}
			username = username.trim();
			if (username === "" || password === "") {
				if (username === "" && password === "") {
					alert("Username and password fields cannot be empty.");
					return false;
				}
				else if (username === "") {
					alert("Username field cannot be empty.");
					return false;
				}
				else {
					alert("Password field cannot be empty.");
					return false;
				}
			}
			else {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == 4) {
            if (xhttp.responseText == "SUCCESS") {
              alert("good");
              window.location.assign("index.php");
            }
            else {
              alert(xhttp.responseText);
            }
          }
        }
        xhttp.open("POST", "loginSQL.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var stringToSend = "username=" + username + "&password=" + password;
        xhttp.send(stringToSend);

			}
		}
	</script>
    <script src="index.js"></script>
</html>

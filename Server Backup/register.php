
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#000000" />
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">

<nav class="navbar navbar-inverse navbar-fixed-top">

  <ul class="nav navbar-nav">
    <li><a href=""><img src="dennis.jpg" width="57" height "57"></a></li>
    <li><a href=""><strong>
      <logo><span style="color:white">Pixel</span></a></strong></logo></li>
      <li class="camera-icon">
        <form id="form" action="kent.php" method="post">
          <div class="image-upload">
            <label for="file-input">
              <img src="camera.png" height="30" width="30"/>
            </label>

            <input id="file-input" type="file" accept="image/*" capture="camera">
          </div>
        </form></li>
        <li class="menu-icon"><a href="index.html"><img width="30" height="30" src="browse.png"></a></li>
        <!--<li class="menu-icon"><a href=""><img width="30" height="30" src="search.png"></a></li>-->

      </ul>
    </nav>


    <div class="container">
      <div class="row">
        <div class="new-image-header">
          <p><strong>Create an Account</strong></p>
        </div>

        <!--<div class="col-*-*"><img class="new-image-preview" src="<?php echo $_FILES["fileToUpload"]["name"]; ?>"></div>-->

        <div class="create-an-account">
            <!--<formtitle></formtitle><br/>-->
            <br/><formtitle>Username</formtitle><br/>
            <input type="text" class="form-text" name="username" id="username" style="width:100%" required><br/>
            <br/>
            <formtitle>Password</formtitle><br/>
            <input type="password" class="form-text" name="password" id="password" style="width:100%" required><br/>
            <br/>
			<formtitle>Profile Picture Upload</formtitle><br/>
            <input id="Profile-file-input" type="file" accept="image/*" capture="camera">
			<br/>
            <input type="button" class="form-button" value="Create Account" name="createAccountButton" id="createAccountButton" style="width:100%"><br/>
          <br/>
        </div>

      </div>
    </div>
	<script>
		window.onload = main;

		function main() {
		    document.getElementById("createAccountButton").onclick = validateData; // DO NOT PUT ()
		}
		function validateData() {
			var alertMessage = "";
			var username = document.getElementById("username").value;
			var password = document.getElementById("password").value;
			if (username == null || password == null) {
				alert("Username and password fields cannot be empty.");
				return false;
			}
			username = username.trim();
			if (username === "" || password === "") {
				if (username === "" && password === "") {
					alert("Username and password fields cannot be empty.");
					return false;
				}
				else if (username === "") {
					alert("Username field cannot be empty.");
					return false;
				}
				else {
					alert("Password field cannot be empty.");
					return false;
				}
			}
			else {



        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.responseText == "SUCCESS") {
              alert("Account successfuly created!")
              window.location.replace("index.php");
            }
            else {
              alert(xhttp.responseText);
            }
          }
        }
        xhttp.open("POST", "registerSQL.php", true);

        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var stringToSend = "username=" + username + "&password=" + password;
        xhttp.send(stringToSend);



/*


				$.ajax({
					method: "POST",
					url: 'registerSQL.php',
					data:{username:username, password:password},
					success:function(html) {
						alert(html);
					}
				});
        */
				return true;
			}
		}
	</script>
    <script src="index.js"></script>
    </html>

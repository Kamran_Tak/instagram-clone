<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#000000" />
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">

<?php
error_reporting(E_ERROR);
require_once("support.php");

echo <<<EOBODY
<div class="container">
<div class="row">
<div class="new-image-header">
<p><strong>New Post</strong></p>
</div>
EOBODY;

echo renderMenuBar();
$tmpFileName = $_FILES['imageupload']['tmp_name'];
$serverFileName = "/storage/ssd5/011/2131011/public_html/tempimg/" .$_FILES['imageupload']['name'];

move_uploaded_file($tmpFileName, $serverFileName);
echo "<div class=\"col-*-*\"><img class=\"new-image-preview\" id=\"newImage\" src=\"";

echo "https://pixel-storage.000webhostapp.com/tempimg/" . $_FILES['imageupload']['name'];
echo "\"></div>";
echo <<<EOBODY
<div class="new-image-form">
<form id="formData" name="formData">
<formtitle></formtitle><br/>
<formtitle>Caption</formtitle><br/>
<input type="text" class="form-text" name="caption" style="width:100%"><br/>
<br/>
<formtitle>Category</formtitle><br/>
<select style='width:100%;' name="Category" id="Category">
EOBODY;

$db = connectToDB("localhost", "id2131024_admin", "sprcoredump", "id2131024_pixel");
$sqlQuery = sprintf("select categoryID, name from Categories order by name DESC");
$result = mysqli_query($db, $sqlQuery);

while ($recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
  echo "<option style=\"dropdown-box\" value=\"" . $recordArray["categoryID"] . "\">" . $recordArray["name"] . "</option>";
}

echo <<<EOBODY

</select>
<br/>
<br/>
<formtitle>Location</formtitle><br/>
<select style='width:100%;' name="Location" id="Location">

EOBODY;

$sqlQuery = sprintf("select locationID, name from Locations order by name DESC");
$result = mysqli_query($db, $sqlQuery);

while ($recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
  echo "<option style=\"dropdown-box\" value=\"" . $recordArray["locationID"] . "\">" . $recordArray["name"] . "</option>";
}

echo <<<EOBODY
</select>
<br/><br/><br/><br/>
<input type="button" class="form-button" action="form-button" value="Post" name="uploadImage" onclick=postImage() style="width:100%"><br/>
</form>
<br/>
</div>
</div>
</div>

EOBODY;

mysqli_close($db);
?>

<script src="index.js"></script>
<script>
var xhttp = new XMLHttpRequest();
function postImage() {
  alert("kent");
  xhttp.open("POST", "postimageajax.php", true);
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == XMLHttpRequest.DONE) {
      alert(xhttp.responseText);
    }
  }
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  alert(document.getElementById("Category"));
  var stringToSend = "category=" + document.forms["formData"]["Category"].value + "&location=" + document.forms["formData"]["Location"].value + "&path=" + document.getElementById("newImage").src ;
  alert(stringToSend);
  xhttp.send(stringToSend);
}
</script>
</html>

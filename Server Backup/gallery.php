
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#000000" />
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">

<nav class="navbar navbar-inverse navbar-fixed-top">

  <ul class="nav navbar-nav">
    <li><a href=""><img src="dennis.jpg" width="57" height "57"></a></li>
    <li><a href=""><strong>
      <logo><span style="color:white">Pixel</span></a></strong></logo></li>
      <li class="camera-icon">
        <form id="form" action="kent.php" method="post">
          <div class="image-upload">
            <label for="file-input">
              <img src="camera.png" height="30" width="30"/>
            </label>

            <input id="file-input" type="file" accept="image/*" capture="camera">
          </div>
        </form></li>
        <li class="menu-icon"><a href="index.html"><img width="30" height="30" src="browse.png"></a></li>
        <li class="menu-icon"><a href=""><img width="30" height="30" src="search.png"></a></li>

      </ul>
    </nav>


    <div class="container">
      <div class="row">
        <div class="new-image-divider">
          <br/>
          <p><strong>Funny</strong></p>
        </div>
        <div class="image-grid">
			<?php
				require 'SQLFunctions.php';
				$db = new SQLFunctions("localhost", "id2131024_admin", "sprcoredump", "id2131024_pixel");
				#$categoryID = 1;

				if(isset($_GET['categoryID'])){
					$categoryID = $_GET['categoryID'];
					$images = $db->getImagesByCategory($categoryID);
				}else{
					if(isset($_GET['locationID'])){
						$locationID = $_GET['locationID'];
						$images = $db->getImagesByLocation($locationID);
					}
				}
				$html = "<div class='col-*-*'>";
				if ( mysqli_num_rows($images) > 0) {
					$iter = 0;
					while($row = mysqli_fetch_assoc($images)) {
						$path = $row["path"];
						$id = $row["pictureID"];

						if($iter%3 == 0 && $iter !=0){
							$html = $html . "</div><div class='col-*-*'>";
						}
						$html = $html . "<a href='/viewpost.php?imageID=$id'><img class='grid-image' height='100' src='$path'></a>";
						$iter++;
					}
				}else{
					echo "<label>FAILED</label>";
				}
				echo $html . "</div>";
			?>
        </div>
      <br/>
    </div>
    </div>
    <script src="index.js"></script>
</html>

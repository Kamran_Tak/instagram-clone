<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#000000" />
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">

<?php
error_reporting(E_ERROR);
require_once("support.php");

// Log in to database
$host = "localhost";
$user = "id2131024_admin";
$password = "sprcoredump";
$database = "id2131024_pixel";
$table = "Pictures";
$db = connectToDB($host, $user, $password, $database);

$output = "";

echo renderMenuBar();

$output=<<<EOBODY
<div class="container">
  <div class="row">
    <div class="first-section-divider">
      <p>Recently Uploaded</p>
    </div>
    <div class="image-grid">
EOBODY;

$sqlQuery = sprintf("select pictureID, path from Pictures order by PictureID DESC");
$result = mysqli_query($db, $sqlQuery);
$sqlCount = 0;
for ($row=0; $row<3; $row++) {
  $output .= "<div class=\"col-*-*\">";
  for ($column=0; $column<3; $column++) {
    $recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $output .= "<a href=\"/viewpost.php?imageID=". $recordArray["pictureID"] . "\"><img class=\"grid-image\" src=\"" . $recordArray["path"] . "\"></a>";
    $sqlCount++;
  }
  $output .= "</div>";
}

$output.=<<<EOBODY
<div class="section-divider">
  <p>Categories</p>
</div>
EOBODY;

$sqlQuery = sprintf("select name,categoryID from Categories order by name DESC");
$result = mysqli_query($db, $sqlQuery);

while ($recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
  $output .= "\n<a href=\"gallery.php?categoryID=" . $recordArray["categoryID"] . "\"><div class=\"category-item\">" . $recordArray["name"]. "</div></a><hr/>";
}

$output.=<<<EOBODY
<div class="section-divider">
  <p>Locations</p>
</div>
EOBODY;


$sqlQuery = sprintf("select name,locationID from Locations order by name DESC");
$result = mysqli_query($db, $sqlQuery);

while ($recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
  $output .= "\n<a href=\"gallery.php?locationID=" . $recordArray["locationID"] . "\"><div class=\"location-item\">" . $recordArray["name"] . "</div></a>";
}

mysqli_close($db);
echo $output;

?>

</div>
</div>
<script src="index.js"></script>
</html>

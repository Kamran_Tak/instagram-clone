<?php

//if(!isset($_SESSION)) {
//	session_start();
//}

Class SQLFunctions {
	//grant all on xd.* to pixel@"localhost" identified by "asdf"
	private $host;
	private $user;
	private $password;
	private $database;
	private $db;

	function __construct($host, $user, $password, $database)  {
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
		$this->database = $database;
		$this->connectToDB();
	}

	function field_Generator($fieldName, $fieldType, $fieldAttributes) {
		if ($fieldAttributes == null) {
			return $fieldName.' '.$fieldType.', ';
		}
		return $fieldName.' '.$fieldType.' '.$fieldAttributes.', ';
	}


	function connectToDB() {
		$this->db = mysqli_connect($this->host, $this->user, $this->password, $this->database);
		if (mysqli_connect_errno()) {
			exit("Connect failed.\n".mysqli_connect_error());
		}
	}

	function closeConnection() {
		mysqli_close($this->db);
	}

	function insertIntoTable($tableName, $vars, $fieldValues) {
		$this->connectToDB();
		$insertIntoTableCommand = "insert into $tableName ($vars) values($fieldValues);";
		$result = mysqli_query($this->db, $insertIntoTableCommand);
		$id = mysqli_insert_id($this->db);
		if ($result) {
			return $id;
		}
		else {
			echo $insertIntoTableCommand."<br>";
			echo mysqli_error($this->db);
			return -1;
		}
	}


	function getInfoFromUsername($username) {
		//$tablename = "users";
		//$selection = "username, password, imagePath";
		//$where = "username = '$username'";
		//$result = $this->selectFromTable($tablename, $selection, $where);
		//

		$this->connectToDB();
		$sqlQuery = "select userID,username, password, imagePath from Users where username = '$username';";
		$result = mysqli_query($this->db, $sqlQuery);

		if ($result) {
			$numRows = mysqli_num_rows($result);
			if ($numRows > 0) {
				$result->data_seek(0);
				$entry = $result->fetch_array(MYSQLI_ASSOC);
				return $entry;
				//$recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC);
				//return $recordArray;
			}
			else {
				return 0;
			}
		}
		else {
			echo "FAILED";
			return -1;
		}
	}



	function selectFromTable($tablename, $selection, $where) {
		$this->connectToDB();
		$sqlQuery = "select $selection from $tablename where $where;";
		$result = mysqli_query($this->db, $sqlQuery);
		if ($result) {
			return $result;
		}
		else {
			echo $sqlQuery;
			echo mysqli_error($this->db);
			return false;
		}
	}

	//UPDATE table_name
	//SET column1 = value1, column2 = value2, ...
	//WHERE condition;
	function updateTable($tablename, $set, $where) {
		$this->connectToDB();
		$sqlQuery = "update $tablename set $set where $where;";
		$result = mysqli_query($this->db, $sqlQuery);
		if ($result) {
			return $result;
		}
		else {
			echo $sqlQuery;
			echo mysqli_error($this->db);
			return false;
		}

	}

		function insertUser($username, $password, $imagePath) {
			$hashedPW = password_hash($password, PASSWORD_DEFAULT); //SHOULDNT BE COMMENTED********************
				$this->connectToDB();
				$insertIntoTableCommand = "insert into Users (username, password, imagePath) values('$username', '$hashedPW', '$imagePath');";
				$result = mysqli_query($this->db, $insertIntoTableCommand);
				$id = mysqli_insert_id($this->db);
				if ($result) {
					return $id;
				}
				else {
					echo $insertIntoTableCommand."<br>";
					echo mysqli_error($this->db);
					return -1;
				}
			}





	function insertImage($timestamp, $views, $userID, $categoryID, $locationID, $path, $caption) {
		$q = "\"";
		$values = $timestamp.", ".$views.", ".$userID.", ".$categoryID.", ".$locationID.", ".$q.$path.$q.", ".$q.$caption.$q;
		$vars = "timestamp, views, userID, categoryID, locationID, path, caption";
		$id = $this->insertIntoTable("Pictures", $vars, $values);

		if ($id == -1) {
			return 0;
		} else {
			return $id;
		}
	}

	function addView($pictureID) {
		$views = $this->getViews($pictureID);
		$tablename = "Pictures";
		$set = "Views=".($views + 1);
		$where = "pictureID = ".$pictureID;
		$result = $this->updateTable($tablename, $set, $where);
		if ($result) {
			echo $this->getViews();
		}
		else {
			echo "FAILED";
		}
	}
	function addView2($pictureID) {
		$views = $this->getViews($pictureID);
		$tablename = "Pictures";
		$set = "Views=".($views + 1);
		$where = "pictureID = ".$pictureID;
		$result = $this->updateTable($tablename, $set, $where);
		if ($result) {
			return $this->getViews($pictureID);
		}
		else {
			echo ($result);
			return -1;
		}
	}
	function getViews($pictureID) {
		$tablename = "Pictures";
		$selection = "Views";
		$where = "pictureID = ".$pictureID;
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			$recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$views = $recordArray['Views'];
			return $views;
		}
		else {
			echo "FAILED";
			return -1;
		}
	}

	function getImageInfo($pictureID) {
			$tablename = "Pictures";
			$selection = "timestamp, userID, categoryID, locationID, path, `caption`";
			$where = "pictureID = ".$pictureID;
			$result = $this->selectFromTable($tablename, $selection, $where);

			if ($result) {
				$recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC);
				return $recordArray;
			}
			else {
				echo "FAILED";
				return -1;
			}
		}

	//get locationName from locationID
	function getLocationName($locationID) {
		$tablename = "Locations";
		$selection = "(name)";
		$where = "locationID = ".$locationID;
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			$recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC);
			return $recordArray['name'];
		}
		else {
			echo "FAILED";
			return -1;
		}
	}

	//get profilePicturePath, username from userID
	function getUserInfo($userID) {
		$tablename = "Users";
		$selection = "username, imagePath";
		$where = "userID = ".$userID;
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			$recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC);
			return $recordArray;
		}
		else {
			echo "FAILED";
			return -1;
		}
	}

	//get categoryName from categoryID
	function getCategoryName($categoryID) {
		$tablename = "Categories";
		$selection = "(name)";
		$where = "categoryID = ".$categoryID;
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			$recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC);
			return $recordArray['name'];
		}
		else {
			echo "FAILED";
			return -1;
		}
	}

	//get (comment) texts from imageID
	function getCommentText($imageID) {
		$tablename = "Comments";
		$selection = "(text,userID)";
		$where = "pictureID = ".$imageID;
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			$recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC);
			return $recordArray;
		}
		else {
			echo "FAILED";
			return -1;
		}
	}

		//get (comment) texts from imageID
	function getComments($imageID) {
		$tablename = "Comments JOIN Users ON Comments.userID = Users.userID";
		$selection = "text,username";
		$where = "pictureID = ".$imageID;
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			return $result;
		}
		else {
			echo "FAILED";
			return -1;
		}
	}

	function getLikes($imageID){
		$tablename = "Likes";
		$selection = "userID";
		$where = "pictureID = ".$imageID;
		$result = $this->selectFromTable($tablename,$selection,$where);

		if($result){
			return $result;
		}else{
			return 0;
		}
	}

	function hasLiked($userID,$imageID){
		$tablename = "Likes";
		$selection = "userID";
		$where = "pictureID = ".$imageID;
		$where = $where . " AND userID = " . $userID;
		$result = $this->selectFromTable($tablename,$selection,$where);
		if($result){
			return mysqli_num_rows($result);
		}else{
			echo "failed";
			return 0;
		}
	}

	function deleteLike($userID,$imageID){
		$tablename = "Likes";
		$selection = "userID";
		$where = "pictureID = ".$imageID;
		$where = $where . " AND userID = " . $userID;

		$this->connectToDB();
		$sqlQuery = "delete from Likes where pictureID=$imageID and userID=$userID;";
		return mysqli_query($this->db, $sqlQuery);
	}

	function insertLike($userID,$imageID){
		return $this->insertIntoTable('Likes', 'userID, pictureID', "$userID ,$imageID");
	}

	function insertComment($userID,$imageID,$text){
		return $this->insertIntoTable('Comments', 'userID, pictureID, text', "$userID ,$imageID ,'$text'");
	}
	function getImagesByCategory($categoryID){
		$tablename = "Pictures";
		$selection = "path,pictureID";
		$where = "categoryID = ".$categoryID;
		$where = $where . " ORDER BY pictureID DESC";
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			return $result;
		}
		else {
			echo "FAILED";
			return -1;
		}
	}

	function getImagesByLocation($locationID){
		$tablename = "Pictures";
		$selection = "path,pictureID";
		$where = "locationID = ".$locationID;
		$where = $where . " ORDER BY pictureID DESC";
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			return $result;
		}
		else {
			echo "FAILED";
			return -1;
		}
	}

	function getLastInsertedID() {
		return $this->db->insert_id;
	}

	function getImagesByUser($userID){
		$tablename = "Pictures";
		$selection = "path,pictureID";
		$where = "userID = ".$userID;
		$where = $where . " ORDER BY pictureID DESC";
		$result = $this->selectFromTable($tablename, $selection, $where);

		if ($result) {
			return $result;
		}
		else {
			echo "FAILED";
			return -1;
		}
	}
}
?>

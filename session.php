<?php
	session_start();
	if(!isset($_SESSION['username'])){
		header('Location: login.php');
	}
	if(!isset($_SESSION['password'])){
		header('Location: login.php');
	}

	$username = $_SESSION['username'];
	$password = $_SESSION['password'];

	require_once 'SQLFunctions.php';
	$db = new SQLFunctions("localhost", "id2131024_admin", "sprcoredump", "id2131024_pixel");

	$user= $db -> getInfoFromUsername($username);
	$dbpass = $user['password'];
	$verified = password_verify($password, $dbpass);
	if(!$verified){
		header('Location: login.php');

	}
?>

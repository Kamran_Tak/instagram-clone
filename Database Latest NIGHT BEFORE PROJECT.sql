-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 05, 2017 at 07:22 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id2131024_pixel`
--

-- --------------------------------------------------------

--
-- Table structure for table `Categories`
--

CREATE TABLE `Categories` (
  `categoryID` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `color` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Categories`
--

INSERT INTO `Categories` (`categoryID`, `name`, `color`) VALUES
(1, 'Funny', 'red'),
(2, 'Food', 'red'),
(3, 'Nature', 'red'),
(4, 'Sports', 'red'),
(5, 'NSCF', 'red');

-- --------------------------------------------------------

--
-- Table structure for table `Comments`
--

CREATE TABLE `Comments` (
  `commentID` int(10) UNSIGNED NOT NULL,
  `text` varchar(1000) DEFAULT NULL,
  `pictureID` int(11) DEFAULT NULL,
  `userID` int(10) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Comments`
--

INSERT INTO `Comments` (`commentID`, `text`, `pictureID`, `userID`) VALUES
(1, 'Fuck', 28, 1),
(2, 'Kamrin is a bitch', 30, 1),
(3, 'Hkkmb', 28, 1),
(4, 'OR 1=1', 28, 1),
(5, '\" OR 1=1', 28, 1),
(6, '\" OR 1=1', 28, 1),
(7, 'Autism speaks', 29, 1),
(8, 'Autism has spoken..', 29, 1),
(9, 'Jdjfn', 30, 1),
(10, 'Jdjfn', 30, 1),
(11, 'kent likes hentai!!! :))))))', 45, 1),
(12, 'I love kent', 47, 1),
(13, 'I love runescape', 55, 1),
(14, 'I love runescape', 55, 1),
(15, 'I love runescape', 55, 1),
(16, 'Chill', 60, 40),
(17, 'me too thanks', 55, 43);

-- --------------------------------------------------------

--
-- Table structure for table `Likes`
--

CREATE TABLE `Likes` (
  `userID` int(10) UNSIGNED NOT NULL,
  `pictureID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Likes`
--

INSERT INTO `Likes` (`userID`, `pictureID`) VALUES
(1, 20),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 28),
(1, 29),
(1, 47),
(1, 49),
(2, 49),
(2, 50),
(7, 52),
(42, 62),
(43, 55);

-- --------------------------------------------------------

--
-- Table structure for table `Locations`
--

CREATE TABLE `Locations` (
  `locationID` int(10) UNSIGNED NOT NULL,
  `name` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Locations`
--

INSERT INTO `Locations` (`locationID`, `name`) VALUES
(1, 'UMD'),
(2, 'D.C.');

-- --------------------------------------------------------

--
-- Table structure for table `Pictures`
--

CREATE TABLE `Pictures` (
  `pictureID` int(10) UNSIGNED NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `views` int(10) UNSIGNED DEFAULT NULL,
  `userID` int(10) UNSIGNED NOT NULL,
  `categoryID` int(10) UNSIGNED DEFAULT NULL,
  `locationID` int(10) UNSIGNED DEFAULT NULL,
  `path` varchar(1000) DEFAULT NULL,
  `caption` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Pictures`
--

INSERT INTO `Pictures` (`pictureID`, `timestamp`, `views`, `userID`, `categoryID`, `locationID`, `path`, `caption`) VALUES
(1, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg', 'caption'),
(2, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg', 'caption'),
(3, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg', 'caption'),
(4, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg', 'caption'),
(5, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg', 'caption'),
(6, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg', 'caption'),
(20, '2017-07-04 00:00:00', 4, 1, 2, 2, 'http://kent.com/hentee.png', 'caption'),
(21, '2017-07-04 00:00:00', 1, 1, 5, 1, 'http://kent.com/hentee.png', 'caption'),
(22, '2017-07-04 00:00:00', 17, 1, 5, 2, 'https://pixel-storage.000webhostapp.com/tempimg/Menaced Assassin.jpg', 'caption'),
(23, '2017-07-04 00:00:00', 8, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/Menaced Assassin.jpg', 'caption'),
(24, '2017-07-04 00:00:00', 29, 1, 3, 2, 'https://pixel-storage.000webhostapp.com/tempimg/Gishump.png', 'caption'),
(25, '2017-07-04 00:00:00', 9, 1, 3, 2, 'https://pixel-storage.000webhostapp.com/tempimg/1499136975244239327911.jpg', 'caption'),
(26, '2017-07-04 00:00:00', 76, 1, 2, 2, 'https://pixel-storage.000webhostapp.com/tempimg/14991371268501103747248.jpg', 'caption'),
(27, '2017-07-04 00:00:00', 7, 1, 1, 2, 'https://pixel-storage.000webhostapp.com/tempimg/14991487452402021261198.jpg', 'caption'),
(28, '2017-07-04 00:00:00', 18, 1, 5, 1, 'https://pixel-storage.000webhostapp.com/tempimg/14991487767231985046397.jpg', 'caption'),
(29, '2017-07-04 00:00:00', 24, 1, 2, 1, 'https://pixel-storage.000webhostapp.com/tempimg/1499148921155-1411009457.jpg', 'caption'),
(30, '2017-07-04 00:00:00', 12, 1, 3, 2, 'https://pixel-storage.000webhostapp.com/tempimg/1499149462527-1899429919.jpg', 'caption'),
(31, '2017-07-04 00:00:00', 4, 1, 4, 2, 'https://pixel-storage.000webhostapp.com/tempimg/1499160608754614153199.jpg', 'caption'),
(32, '2017-07-04 00:00:00', 3, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/1499194562979735103757.jpg', 'caption'),
(33, '2017-07-04 00:00:00', 1, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/14991947300351270482608.jpg', 'caption'),
(34, '2017-07-04 00:00:00', 5, 1, 4, 2, 'https://pixel-storage.000webhostapp.com/tempimg/14991949995391208856218.jpg', 'caption'),
(35, '2017-07-04 00:00:00', 5, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/14991958062711435737375.jpg', 'caption'),
(36, '2017-07-04 00:00:00', 1, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/dennis.jpg', NULL),
(37, '2017-07-04 00:00:00', 1, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/dennis.jpg', NULL),
(38, '2017-07-04 00:00:00', 4, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/dennis.jpg', NULL),
(39, '2017-07-04 00:00:00', 3, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/commentc.png', NULL),
(40, '2017-07-04 00:00:00', 4, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/categoryc.png', NULL),
(41, '2017-07-05 00:00:00', 4, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/Menaced Assassin.jpg', NULL),
(42, '2017-07-05 00:00:00', 4, 1, 4, 2, 'https://pixel-storage.000webhostapp.com/tempimg/Snapchat-4765639824244673924-2.jpg', NULL),
(43, '2017-07-05 00:00:00', 2, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/Gishump.png', NULL),
(44, '2017-07-05 00:00:00', 3, 1, 2, 1, 'https://pixel-storage.000webhostapp.com/tempimg/4483_render_Zack_sword.png', '1'),
(45, '2017-07-05 00:00:00', 29, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/4483_render_Zack_sword.png', 'My sword'),
(46, '2017-07-05 00:00:00', 4, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/1499226102356446971972.jpg', 'I want to off myself'),
(47, '2017-07-05 00:00:00', 6, 1, 1, 2, 'https://pixel-storage.000webhostapp.com/tempimg/1499226365016781930374.jpg', 'Chuck 1, more like cuck 1.. am I right?'),
(48, '2017-07-05 00:00:00', 2, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/1499226624677848096735.jpg', 'Ha Ha Ha'),
(49, '2017-07-05 00:00:00', 19, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/14992271440051369528628.jpg', 'Asdf'),
(50, '2017-07-05 00:00:00', 10, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/14992271764841177176951.jpg', ''),
(51, '2017-07-05 00:00:00', 2, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/camera.png', 'Happy 4th of july'),
(52, '2017-07-05 00:00:00', 9, 7, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/img2.png', 'sdfs'),
(53, '2017-07-05 00:00:00', 2, 7, 3, 1, 'https://pixel-storage.000webhostapp.com/tempimg/categoryc.png', 'Dank porn'),
(54, '2017-07-05 00:00:00', 4, 34, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/grass.jpg', 'Ana'),
(55, '2017-07-05 00:00:00', 14, 7, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/1499231589003917629804.jpg', 'I love crack so much ????'),
(56, '2017-07-05 00:00:00', 6, 7, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/camera.png', 'sdfsdf'),
(57, '2017-07-05 00:00:00', 2, 21, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/dennis.jpg', '530'),
(58, '2017-07-05 00:00:00', 2, 38, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/categoryc.png', 'aaa'),
(59, '2017-07-05 00:00:00', 3, 39, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/browse.png', 'YOU!'),
(60, '2017-07-05 00:00:00', 8, 41, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/dennis.jpg', 'Cash money!!!!!!!!!'),
(61, '2017-07-05 00:00:00', 2, 40, 1, 2, 'https://pixel-storage.000webhostapp.com/tempimg/1499237289145821204755.jpg', 'I watch and beat off to a lot of porn'),
(62, '2017-07-05 00:00:00', 10, 42, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/1499237492921192746474.jpg', 'Dummu'),
(63, '2017-07-05 00:00:00', 10, 41, 5, 1, 'https://pixel-storage.000webhostapp.com/tempimg/sport.jpg', 'Stands');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `userID` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `imagePath` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`userID`, `username`, `password`, `imagePath`) VALUES
(1, 'user1', 'pass', NULL),
(2, 'user2', 'pass', NULL),
(3, 'gishump', '1092Wilda', 'asdf.jpg'),
(4, 'jeffdl', '1092', 'asdf.jpg'),
(5, 'sdfsdf', 'sdfsd', 'asdf.jpg'),
(6, 'chuck', 'cuckhold', 'asdf.jpg'),
(7, 'chuck1', 'cuckhold', 'asdf.jpg'),
(8, 'jasdf', 'asdf', 'asdf.jpg'),
(9, 'chukc', 'asdfasfd', 'asdf.jpg'),
(10, 'cuck1', 'cuckgeppi', 'asdf.jpg'),
(11, 'chuck3', '321', 'asdf.jpg'),
(12, 'chic', 'sdfs', 'asdf.jpg'),
(13, 'asdfdf', 'dfdf', 'asdf.jpg'),
(14, 'sdfsd', 'ads', 'asdf.jpg'),
(15, 'sdfs', 'sdfs', 'asdf.jpg'),
(16, '', '', 'asdf.jpg'),
(17, 'ken', 'wer', 'asdf.jpg'),
(18, 'sdfdffff', 'fff', 'asdf.jpg'),
(19, 'chuck4', '444', 'asdf.jpg'),
(20, 'charles', '1234ass', 'asdf.jpg'),
(21, 'kent', '1092Wilda', 'asdf.jpg'),
(22, 'pornhub', 'hentai', 'asdf.jpg'),
(23, 'austinboyer', 'kent', 'https://pixel-storage.000webhostapp.com/profile/'),
(24, '501', '1092Wilda', 'https://pixel-storage.000webhostapp.com/profile/'),
(25, '42342342', '234234', 'asdf.jpg'),
(26, 'asdfasdfasdfasdfasdfasd', 'sadfsdf', 'asdf.jpg'),
(27, 'asdfasdfasdfasdfsdfdf', 'asdfsasdfdsafdasafadsfdas', 'asdf.jpg'),
(28, 'asdfasdfasdfasdfasdfasdfasdfasdfasdfasd', '342334', 'asdf.jpg'),
(29, 'asdfasdfasdddddd', 'ddddd', 'asdf.jpg'),
(30, 'sadfasdfasdfa', 'asdfasdfasdfas', 'asdf.jpg'),
(31, 'ffttttt', 'ttt', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(32, 'Jackiechan81', '1092Wilda', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(33, 'Kamran', 'admin', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(34, 'charles1092', '1092Wilda', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(35, 'dan', '1234', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(36, 'assoc', 'assoc', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(37, 'associ', 'assoc', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(38, 'asi', 'asi', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(39, 'you', 'you', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(40, 'chill', '$2y$10$IflWTlZhkAyEY0JF2uFHTuh0MxV/l.sASc5uPfs0eYr7Oo7WrFpIe', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(41, 'joey', '$2y$10$ryITb2pQK9xvxkTuzfWpN.tgMk/nWFUSaC7sTLXpdii7aOMs3zAtW', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(42, 'walker', '$2y$10$9Uvwz4y80EVO8qMVz9YHTOErjdf4Hkz1aprWGU84SOMHY51p/rJCm', 'https://pixel-webapp.000webhostapp.com/dennis.jpg'),
(43, 'Kamran1', '$2y$10$IyEYG90dEKwpwZMdKERn8usuX5Z2lSCMn1h9HmD8l0atN//DA/ate', 'https://pixel-webapp.000webhostapp.com/dennis.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`categoryID`);

--
-- Indexes for table `Comments`
--
ALTER TABLE `Comments`
  ADD PRIMARY KEY (`commentID`);

--
-- Indexes for table `Likes`
--
ALTER TABLE `Likes`
  ADD PRIMARY KEY (`userID`,`pictureID`),
  ADD KEY `pictureID` (`pictureID`);

--
-- Indexes for table `Locations`
--
ALTER TABLE `Locations`
  ADD PRIMARY KEY (`locationID`);

--
-- Indexes for table `Pictures`
--
ALTER TABLE `Pictures`
  ADD PRIMARY KEY (`pictureID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `categoryID` (`categoryID`),
  ADD KEY `locationID` (`locationID`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `categoryID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Comments`
--
ALTER TABLE `Comments`
  MODIFY `commentID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `Locations`
--
ALTER TABLE `Locations`
  MODIFY `locationID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Pictures`
--
ALTER TABLE `Pictures`
  MODIFY `pictureID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `userID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Likes`
--
ALTER TABLE `Likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`),
  ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`pictureID`) REFERENCES `Pictures` (`pictureID`);

--
-- Constraints for table `Pictures`
--
ALTER TABLE `Pictures`
  ADD CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`),
  ADD CONSTRAINT `pictures_ibfk_2` FOREIGN KEY (`categoryID`) REFERENCES `Categories` (`categoryID`),
  ADD CONSTRAINT `pictures_ibfk_3` FOREIGN KEY (`locationID`) REFERENCES `Locations` (`locationID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

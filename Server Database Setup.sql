-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 04, 2017 at 09:22 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id2131024_pixel`
--

-- --------------------------------------------------------

--
-- Table structure for table `Categories`
--

CREATE TABLE `Categories` (
  `categoryID` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Categories`
--

INSERT INTO `Categories` (`categoryID`, `name`) VALUES
(1, 'Funny'),
(2, 'Food'),
(3, 'Nature'),
(4, 'Sports'),
(5, 'NSCF');

-- --------------------------------------------------------

--
-- Table structure for table `Comments`
--

CREATE TABLE `Comments` (
  `commentID` int(10) UNSIGNED NOT NULL,
  `text` varchar(1000) DEFAULT NULL,
  `pictureID` int(11) DEFAULT NULL,
  `userID` int(10) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Comments`
--

INSERT INTO `Comments` (`commentID`, `text`, `pictureID`, `userID`) VALUES
(1, 'Fuck', 28, 1),
(2, 'Kamrin is a bitch', 30, 1),
(3, 'Hkkmb', 28, 1),
(4, 'OR 1=1', 28, 1),
(5, '\" OR 1=1', 28, 1),
(6, '\" OR 1=1', 28, 1),
(7, 'Autism speaks', 29, 1),
(8, 'Autism has spoken..', 29, 1),
(9, 'Jdjfn', 30, 1),
(10, 'Jdjfn', 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Likes`
--

CREATE TABLE `Likes` (
  `userID` int(10) UNSIGNED NOT NULL,
  `pictureID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Likes`
--

INSERT INTO `Likes` (`userID`, `pictureID`) VALUES
(1, 20),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 28),
(1, 29);

-- --------------------------------------------------------

--
-- Table structure for table `Locations`
--

CREATE TABLE `Locations` (
  `locationID` int(10) UNSIGNED NOT NULL,
  `name` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Locations`
--

INSERT INTO `Locations` (`locationID`, `name`) VALUES
(1, 'UMD'),
(2, 'D.C.');

-- --------------------------------------------------------

--
-- Table structure for table `Pictures`
--

CREATE TABLE `Pictures` (
  `pictureID` int(10) UNSIGNED NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `views` int(10) UNSIGNED DEFAULT NULL,
  `userID` int(10) UNSIGNED NOT NULL,
  `categoryID` int(10) UNSIGNED DEFAULT NULL,
  `locationID` int(10) UNSIGNED DEFAULT NULL,
  `path` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Pictures`
--

INSERT INTO `Pictures` (`pictureID`, `timestamp`, `views`, `userID`, `categoryID`, `locationID`, `path`) VALUES
(1, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg'),
(2, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg'),
(3, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg'),
(4, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg'),
(5, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg'),
(6, '2017-07-01 00:00:00', 1, 1, 1, 1, '/images/image1.jpg'),
(20, '2017-07-04 00:00:00', 4, 1, 2, 2, 'http://kent.com/hentee.png'),
(21, '2017-07-04 00:00:00', 1, 1, 5, 1, 'http://kent.com/hentee.png'),
(22, '2017-07-04 00:00:00', 16, 1, 5, 2, 'https://pixel-storage.000webhostapp.com/tempimg/Menaced Assassin.jpg'),
(23, '2017-07-04 00:00:00', 8, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/Menaced Assassin.jpg'),
(24, '2017-07-04 00:00:00', 29, 1, 3, 2, 'https://pixel-storage.000webhostapp.com/tempimg/Gishump.png'),
(25, '2017-07-04 00:00:00', 9, 1, 3, 2, 'https://pixel-storage.000webhostapp.com/tempimg/1499136975244239327911.jpg'),
(26, '2017-07-04 00:00:00', 75, 1, 2, 2, 'https://pixel-storage.000webhostapp.com/tempimg/14991371268501103747248.jpg'),
(27, '2017-07-04 00:00:00', 7, 1, 1, 2, 'https://pixel-storage.000webhostapp.com/tempimg/14991487452402021261198.jpg'),
(28, '2017-07-04 00:00:00', 18, 1, 5, 1, 'https://pixel-storage.000webhostapp.com/tempimg/14991487767231985046397.jpg'),
(29, '2017-07-04 00:00:00', 24, 1, 2, 1, 'https://pixel-storage.000webhostapp.com/tempimg/1499148921155-1411009457.jpg'),
(30, '2017-07-04 00:00:00', 12, 1, 3, 2, 'https://pixel-storage.000webhostapp.com/tempimg/1499149462527-1899429919.jpg'),
(31, '2017-07-04 00:00:00', 4, 1, 4, 2, 'https://pixel-storage.000webhostapp.com/tempimg/1499160608754614153199.jpg'),
(32, '2017-07-04 00:00:00', 3, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/1499194562979735103757.jpg'),
(33, '2017-07-04 00:00:00', 1, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/14991947300351270482608.jpg'),
(34, '2017-07-04 00:00:00', 2, 1, 4, 2, 'https://pixel-storage.000webhostapp.com/tempimg/14991949995391208856218.jpg'),
(35, '2017-07-04 00:00:00', 1, 1, 4, 1, 'https://pixel-storage.000webhostapp.com/tempimg/14991958062711435737375.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `userID` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `imagePath` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`userID`, `username`, `password`, `imagePath`) VALUES
(1, 'user1', 'pass', NULL),
(2, 'user2', 'pass', NULL),
(3, 'gishump', '1092Wilda', 'asdf.jpg'),
(4, 'jeffdl', '1092', 'asdf.jpg'),
(5, 'sdfsdf', 'sdfsd', 'asdf.jpg'),
(6, 'chuck', 'cuckhold', 'asdf.jpg'),
(7, 'chuck1', 'cuckhold', 'asdf.jpg'),
(8, 'jasdf', 'asdf', 'asdf.jpg'),
(9, 'chukc', 'asdfasfd', 'asdf.jpg'),
(10, 'cuck1', 'cuckgeppi', 'asdf.jpg'),
(11, 'chuck3', '321', 'asdf.jpg'),
(12, 'chic', 'sdfs', 'asdf.jpg'),
(13, 'asdfdf', 'dfdf', 'asdf.jpg'),
(14, 'sdfsd', 'ads', 'asdf.jpg'),
(15, 'sdfs', 'sdfs', 'asdf.jpg'),
(16, '', '', 'asdf.jpg'),
(17, 'ken', 'wer', 'asdf.jpg'),
(18, 'sdfdffff', 'fff', 'asdf.jpg'),
(19, 'chuck4', '444', 'asdf.jpg'),
(20, 'charles', '1234ass', 'asdf.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`categoryID`);

--
-- Indexes for table `Comments`
--
ALTER TABLE `Comments`
  ADD PRIMARY KEY (`commentID`);

--
-- Indexes for table `Likes`
--
ALTER TABLE `Likes`
  ADD PRIMARY KEY (`userID`,`pictureID`),
  ADD KEY `pictureID` (`pictureID`);

--
-- Indexes for table `Locations`
--
ALTER TABLE `Locations`
  ADD PRIMARY KEY (`locationID`);

--
-- Indexes for table `Pictures`
--
ALTER TABLE `Pictures`
  ADD PRIMARY KEY (`pictureID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `categoryID` (`categoryID`),
  ADD KEY `locationID` (`locationID`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `categoryID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Comments`
--
ALTER TABLE `Comments`
  MODIFY `commentID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `Locations`
--
ALTER TABLE `Locations`
  MODIFY `locationID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Pictures`
--
ALTER TABLE `Pictures`
  MODIFY `pictureID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `userID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Likes`
--
ALTER TABLE `Likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`),
  ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`pictureID`) REFERENCES `Pictures` (`pictureID`);

--
-- Constraints for table `Pictures`
--
ALTER TABLE `Pictures`
  ADD CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`),
  ADD CONSTRAINT `pictures_ibfk_2` FOREIGN KEY (`categoryID`) REFERENCES `Categories` (`categoryID`),
  ADD CONSTRAINT `pictures_ibfk_3` FOREIGN KEY (`locationID`) REFERENCES `Locations` (`locationID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
  require "session.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#000000" />
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">

<?php

  require_once("support.php");
  echo renderMenuBar();
 ?>

    <div class="container">
      <div class="row">

        <div class="image-grid">
			<?php
				require_once 'SQLFunctions.php';
				$db = new SQLFunctions("localhost", "id2131024_admin", "sprcoredump", "id2131024_pixel");
				#$categoryID = 1;
				if(isset($_GET["pageNumber"])){
					$pageNumber = $_GET['pageNumber'];
				}else{
					$pageNumber = 1;
				}
				$endIndex = $pageNumber * 9;
				if(isset($_GET['categoryID'])){
					$categoryID = $_GET['categoryID'];
					$images = $db->getImagesByCategory($categoryID);
					$category = $db->getCategoryName($categoryID);
					$get = "?categoryID=$categoryID&pageNumber=$pageNumber";
				}else{
					if(isset($_GET['locationID'])){
						$locationID = $_GET['locationID'];
						$images = $db->getImagesByLocation($locationID);
            $category = $db->getLocationName($locationID);
						$get = "?locationID=$locationID";
					}

				}
				$html = "<div class='new-image-divider'><br/><p><strong>$category</strong></p></div><div class='col-*-*'>";
				if ( mysqli_num_rows($images) > 0) {
					$iter = 0;
					while($row = mysqli_fetch_assoc($images)) {
						if($iter < $endIndex){
							if($iter >= ($endIndex - 9) ){
								$path = $row["path"];
								$id = $row["pictureID"];

								if($iter%3 == 0 && $iter !=0){
									$html = $html . "</div><div class='col-*-*'>";
								}
								$html = $html . "<a href='/viewpost.php?imageID=$id'><img class='grid-image' height='100' src='$path'></a>";

							}
						}else{
							break;
						}
            $iter++;
					}
				}else{
					echo "<label>FAILED</label>";
				}
				echo $html . "</div>";
				$next = $pageNumber+1;
				$prev = $pageNumber-1;
				echo "<br/><div style=\"color: white\"><div style=\"float: right;\"><a href='gallery.php$get&pageNumber=$prev'>Previous Page</a> | Page $pageNumber |
				<a href='gallery.php$get&pageNumber=$next'>Next Page &nbsp</a></p></div></div>";
			?>
        </div>
      <br/>
    </div>
    </div>
    <script src="index.js"></script>
</html>

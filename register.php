
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#000000" />
</head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">

    <div class="container">
      <div class="row">
        <br/>
          <p style="font-size:1.9em; color:white; text-align:center;"><strong>Pixel Account Registration</strong></p>

        <!--<div class="col-*-*"><img class="new-image-preview" src="<?php echo $_FILES["fileToUpload"]["name"]; ?>"></div>-->

        <div class="loginform">
            <!--<formtitle></formtitle><br/>-->
            <br/><formtitle class="loginform">Username</formtitle><br/>
            <input type="text" class="form-text" name="username" id="username" style="width:100%" required><br/>
            <br/>
            <formtitle class="loginform">Password</formtitle><br/>
            <input type="password" class="form-text" name="password" id="password" style="width:100%" required><br/>
            <br/>

			<?php
      /*<formtitle class="loginform">Profile Picture Upload</formtitle><br/>
            <input id="profileimage" type="file" accept="image/*" capture="camera">
			<br/>*/
      ?>
            <input type="button" class="form-button" value="Create Account" name="createAccountButton" id="createAccountButton" style="width:100%"><br/>
      <br/><center><a style="color:yellow; text-decoration:none;" href="login.php">Sign in with existing account</a></center><br/>
        </div>

      </div>
    </div>
	<script>
		window.onload = main;

		function main() {
		    document.getElementById("createAccountButton").onclick = validateData; // DO NOT PUT ()
		}
		function validateData() {
			var alertMessage = "";
			var username = document.getElementById("username").value;
			var password = document.getElementById("password").value;
			if (username == null || password == null) {
				alert("Username and password fields cannot be empty.");
				return false;
			}
			username = username.trim();
			if (username === "" || password === "") {
				if (username === "" && password === "") {
					alert("Username and password fields cannot be empty.");
					return false;
				}
				else if (username === "") {
					alert("Username field cannot be empty.");
					return false;
				}
				else {
					alert("Password field cannot be empty.");
					return false;
				}
			}
			else {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.responseText == "SUCCESS") {
              alert("Account successfuly created!")
              window.location.replace("index.php");
            }
            else {
              alert(xhttp.responseText);
            }
          }
        }
        xhttp.open("POST", "registerSQL.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var stringToSend = "username=" + username + "&password=" + password;
        xhttp.send(stringToSend);
		}
  }
	</script>
    <script src="index.js"></script>
    </html>
